import config from './config/user'

import axios from '@/utils/request'

// 注册
export const reg = (options) => axios.post(config.reg,options)

// 权限 +用户信息

export const login = (options) => axios.post(config.login,options)

export const validate = () => axios.get(config.validate)