export const SET_SLIDERS = 'SET_SLIDERS'

export const USER_LOGIN = 'USER_LOGIN';
export const SET_USER = 'SET_USER'
export const SET_PERMISSION = 'SET_PERMISSION'

export const USER_VALIDATE = 'USER_VALIDATE'
export const USER_LOGOUT = 'USER_LOGOUT'


// 设置路由权限【菜单权限】
export const SET_MENU_PERMISSION = 'SET_MENU_PERMISSION';
// 添加路由动作
export const ADD_ROUTE = 'ADD_ROUTE'

//创建ws
export const CREATE_WEBSOCKET = 'CREATE_WEBSOCKET'
// 设置ws
export const SET_MESSAGE = 'SET_MESSAGE'