import { getSlider } from "../api/public";
import WS from '@/utils/websocket'
import * as types from "./action-types";
const rootModule = {
  state: {
    sliders: [],
  },
  mutations: {
    [types.SET_SLIDERS](state, payload) {
      state.sliders = payload;
    },
    [types.CREATE_WEBSOCKET](state,ws){//存放ws信息
        state.ws = ws
    },
    [types.SET_MESSAGE](state,msg){//存放msg信息
        state.message = msg
    }
  },
  actions: {
    async [types.SET_SLIDERS]({ commit }) {
      let { data } = await getSlider();
      commit(types.SET_SLIDERS,data)
    },

  },
  modules: {},
};

export default rootModule;
