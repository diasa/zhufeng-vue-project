import * as user from '@/api/user'
import * as types from '../action-types'
import {setLocal,getLocal} from '@/utils/local'
import router from '@/router'
import per from '@/router/per.js'

const filterRouter = (authList) => {
    let auths = authList.map(auth => auth.auth)
    function filter(routes){
        return routes.filter(route=>{
            if(auths.includes(route.meta.auth)){//auths匹配上的就会有相对应的菜单
                if(route.children){//如果有娃儿的话就继续匹配
                    route.children = filter(route.children )
                }
                return route;
            }
        })
    }
}
export default {
    state:{
        userInfo:{},
        hasPermission:false,
        menuPermission:false
    },
    mutations:{
        [types.SET_USER](state,userInfo){
            state.userInfo = userInfo
            if(userInfo && userInfo.token){
                setLocal('token',userInfo.token)
            }else{
                localStorage.clear('token')
            }
        },
        [types.SET_PERMISSION](state,has){
            state.hasPermission = has
        },
        [types.SET_MENU_PERMISSION](state,has){
            state.menuPermission = has
        }
    },
    actions:{
        async [types.SET_USER]({commit},{payload,permission}){
            commit(types.SET_USER,payload)
            commit(types.SET_PERMISSION,permission)
        },
        async [types.USER_LOGIN]({commit,dispatch},payload){
            try{
                let result = await user.login(payload)
                dispatch(types.SET_USER,{payload:result.data,permission:true})
            }catch(e){
                return Promise.reject(e)
            }
        },
        async [types.USER_VALIDATE]({dispatch}){
            if(!getLocal('token')) return false
            try { 
                let result = await user.validate()
                dispatch(types.SET_USER,{payload:result.data,permission:true})
                return true
            } catch (e) {
                dispatch(types.SET_USER,{payload:{},permission:false})
                return false
            }
        },
        async [types.USER_LOGOUT]({dispatch}){
            dispatch(types.SET_USER,{payload:{},permission:false})
        },
        async [types.ADD_ROUTE]({commit,state}){
            // 后端返回的用户的权限
            let authList = state.userInfo.authList; 
            //console.log(authList,per)//authList代表着后端返回的数据，per是前端设置的路径
            if(authList){ // 通过权限过滤出当前用户的路由--不同的用户有不同的菜单
                let routes = filterRouter(authList); 
                // 找到manager路由作为第一级 如/manager/articleManager.vue
               //在路由的配置信息上进行查找 --- manager这一条
                let route = router.options.routes.find(item=>item.path === '/manager');
                route.children = routes; // 给manager添加儿子路由
                router.addRoutes([route]); // 动态添加进去
                commit(types.SET_MENU_PERMISSION,true); // 权限设置完毕
            }else{
                commit(types.SET_MENU_PERMISSION,true); // 权限设置完毕
            }
        }
    }
}