import {createNamespacedHelpers} from 'vuex'

let {mapState} = createNamespacedHelpers('user')

export default {
    data(){
        return {list:[]}
    },
    computed:{
        ...mapState(['userInfo'])
    },
    methods:{
        // 生成一个树形结构
        getMenuList(authList){
            let menu = []
            let map = {}
            authList.forEach(m => {
                m.children = []
                map[m.id] = m
                if(m.pid == -1){
                    menu.push(m)
                }else{
                    map[m.pid] && map[m.pid].children.push(m)
                }
            })
            return menu
        }
    },
    mounted(){
        this.list = this.getMenuList(this.userInfo.authList)
    },
    render() {
        let renderChildren = (list) => {
            return list.map(child => {
                return child.children.length ? 
                <el-submenu index={child._id}>
                    <div slot="title">{child.name}</div>
                    {/* 如果有孩子的话继续渲染 -- 递归 */}
                    {renderChildren(child.children)}
                </el-submenu>:
                //自己对应当前的路径
                <el-menu-item index={child.path}>{child.name}</el-menu-item>
            })
        }
        return <el-menu
            background-color="#333"
            text-color="#fff"
            active-text-color="#ffd04b"
            router={true}
        >
            {renderChildren(this.list)}
      </el-menu> 

    }
}