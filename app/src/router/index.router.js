export default [
    {
        path:'/',
        component:()=>import( /*webpackChunkName:'home'*/ '@/views/Home.vue')
    },
    {
        path:'*',// 这个* 会被处理到当前所有路由的最后面，与位置无关
        component:()=>import(/*webpackChunkName:'404'*/ '@/views/404.vue')
    },
    {
        path:'/manager',
        component:()=>import(/*webpackChunkName:'404'*/'@/views/manager/index.vue'),
        meta:{
            needLogin:true
        }
    }
]